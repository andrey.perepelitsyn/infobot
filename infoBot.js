'use strict';

const debug = require('debug')('infoBot');

const WSBinance = require('./connectors/binance');
const TimeWindow = require('./time-window');
const Operations = require('./math');
const Bot = require('./bot');
const { btc, usd } = require('./utility');
const config = require('./config.json');

const timeWindow = new TimeWindow(60000, new Operations.TradeOperations());

function onTrade(trade) {
	// debug('onTrade() got', trade);
	timeWindow.add(trade, trade.tradeTime);
	const t = timeWindow.getTotals();
	debug(usd(t.price), btc(t.quantityBuy), btc(t.quantitySell), btc(t.quantity), usd(t.volumeBuy, 10), usd(t.volumeSell, 10), usd(t.volume, 10), t.ohlc, t.windowSize);
}

main();

async function main() {
	try {
		const bot = new Bot(config.token);
		await bot.loadContext(config.contextPath);
		bot.addAdmin(config.admin);
		process.on('SIGINT', () => { onInterrupt(bot) });
		bot.run();
		const wsBinance = new WSBinance('wss://stream.binance.com:9443/ws/btcusdt@trade', { onTrade: bot.onTrade.bind(bot) });
	} catch(e) {
		debug(e);
	}
}

async function onInterrupt(bot) {
	await bot.saveContext();
	process.exit(0);
}