'use strict';

const WSConnector = require('./ws-connector');

class WSBinance extends WSConnector {
	tryMessageAsTrade(msg) {
		const price = parseFloat(msg.p);
		const quantityBTC = parseFloat(msg.q);
		const quantity = Math.round(quantityBTC * 1e8);
		return msg.e == 'trade' ? {
			id:        msg.t,
			eventTime: msg.E,
			tradeTime: msg.T,
			symbol:    msg.s,
			price,
			quantity,
			volume:    price * quantityBTC,
			isBuyerMM: msg.m
		} : null;
	}
}

module.exports = WSBinance;
