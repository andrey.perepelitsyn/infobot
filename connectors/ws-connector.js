'use strict';

const WebSocket = require('ws');
const debug = require('debug')('ws-connector');

class WSConnector {
	handlers = {
		onTrade: () => debug('default onTrade()')
	};

	constructor(baseEndpoint, handlers, timeout = 300000, reconnectPause = 3000) {
		this.baseEndpoint = baseEndpoint;
		this.handlers = Object.assign(this.handlers, handlers);
		this.timeout = timeout;
		this.reconnectPause = reconnectPause;
		this.reconnect();
	}

	reconnect() {
		if(this.ws)
			this.ws.terminate();
		this.ws = new WebSocket(this.baseEndpoint);
		this.ws.on('error', this.onError.bind(this));
		this.ws.on('open', this.onOpen.bind(this));
		this.ws.on('message', this.onMessage.bind(this));
		this.ws.on('ping', this.onPing.bind(this));
		this.ws.on('close', this.onClose.bind(this));
	}

	heartbeat() {
		if(this.pingTimeout)
			clearTimeout(this.pingTimeout);
		this.pingTimeout = setTimeout(this.reconnect.bind(this), this.timeout);
	}

	onError(e) {
		debug('got ws error:', e);
		this.pingTimeout = setTimeout(this.reconnect.bind(this), this.timeout);
	}

	onOpen() {
		debug('onOpen');
		this.heartbeat();
	}

	onMessage(msgText) {
		const msg = jsonToData(msgText);
		if(!msg)
			return;
		// debug('onMessage() got', msg);
		let result;
		if(result = this.tryMessageAsTrade(msg))
			return this.handlers.onTrade(result);
	}

	tryMessageAsTrade(msg) {
		debug('tryMessageAsTrade() is abstract!');
	}

	onPing() {
		debug('onPing');
		this.heartbeat();
	}

	onClose() {
		debug('onClose');
		clearTimeout(this.pingTimeout);
		setTimeout(this.reconnect.bind(this), this.reconnectPause);
	}
}

function jsonToData(text) {
	try {
		return JSON.parse(text);
	} catch(e) {
		console.error(e);
		return null;
	}
}

module.exports = WSConnector;
