'use strict';

function strToMs(str) {
	if(typeof str == 'number')
		return str;
	const num = parseInt(str) || 0;
	if(str.slice(-2) == 'ms')
		return num;
	switch(str.slice(-1)) {
		case 's':
			return num * 1000;
		case 'm':
			return num * 60*1000;
		case 'h':
			return num * 60*60*1000;
	}
	return num * 1000;
}

function btc(satoshi, size = 12, padChar = ' ') {
	const satStr = satoshi.toString().padStart(8, '0');
	if(satStr.length == 8)
		return ('0.' + satStr).padStart(size, padChar);
	const noDot = satStr.padStart(size-1, padChar);
	return noDot.slice(0, -8) + '.' + noDot.slice(-8);
}

function usd(x, size = 8, padChar = ' ') { return x === null ? 'null' : x.toFixed(2).padStart(size, padChar); }

module.exports = { strToMs, btc, usd };
