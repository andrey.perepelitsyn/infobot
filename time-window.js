'use strict';

const debug = require('debug')('time-window');

const { strToMs } = require('./utility');

class TimeWindow {
	constructor(size, operations) {
		this.size = strToMs(size);
		this.operations = operations;
		this.data = [];
	}

	add(value, ts = Date.now) {
		if(!this.operations.validate(value)) {
			debug('add(): bad value', value);
			return;
		}
		this.shrink(ts - this.size);
		this.data.push({ ts, value });
		this.operations.onAdd(value, this.data);
	}

	shrink(deleteBefore) {
		while(this.data.length > 0 && this.data[0].ts < deleteBefore) {
			const removed = this.data[0].value;
			this.data.shift();
			this.operations.onRemove(removed, this.data);
		}
	}

	getTotals() {
		return this.operations.getTotals(this.data);
	}

	resize(newSize) {
		debug('resize(): from', this.size, 'to', strToMs(newSize));
		this.size = strToMs(newSize);
		this.shrink(Date.now - this.size);
	}
}

module.exports = TimeWindow;
