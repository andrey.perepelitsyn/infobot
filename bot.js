'use strict';

const fsp = require('fs').promises;
const debug = require('debug')('bot');
const Signals = require('./signals');
const TelegramBot = require('node-telegram-bot-api');

class Bot {
	commandHelp = {
		new:     '/new <signalType> [parameters]\n    create new signal. for help on signal types and parameters send "/help signalType"',
		change:  '/change <signalId> parameters\n    change signal parameters.',
		list:    '/list\n    list created signals',
		delete:  '/delete <signalId>\n    delete signal',
		help:    '/help [command]\n    get command[s] description'
	};

	constructor(token) {
		this.emptyContext();
		this.tgApi = new TelegramBot(token, {polling: true});
	}

	addAdmin(userId) {
		if(!this.context.admins[userId])
			this.context.admins[userId] = {};
	}

	run() {
		this.tgApi.onText(/^\/([a-z]+)(.*)$/i, this.onCommand.bind(this));
	}

	answer(userId, text) {
		debug('sending to user', userId, "message:", text);
		this.tgApi.sendMessage(userId, text);
	}

	onTrade(trade) {
		for(const userId in this.context.users) {
			this.context.users[userId].signals.forEach(signal => {
				signal.onNewValue(trade);
			});
		}
	}

	onCommand(msg, match) {
		if(msg.chat.type != 'private')
			return;
		const userId = msg.chat.id;
		const command = match[1];
		const params = match[2].trim().split(/ +/);
		debug('got from user', userId, 'command:', command, params);

		try {
			if(command != 'list' && params.length < 1) {
				this.help(userId, command);
				return;
			}
			switch(command) {
				case 'new':
					this.newSignal(userId, params[0], params.slice(1));
					break;
				case 'change':
					this.changeSignal(userId, params[0], params.slice(1));
					break;
				case 'on':
					this.turnOnOffSignal(userId, params[0], true);
					break;
				case 'off':
					this.turnOnOffSignal(userId, params[0], false);
					break;
				case 'list':
					this.listSignals(userId);
					break;
				case 'delete':
					this.deleteSignal(userId, params[0]);
					break;
				case 'help':
					this.help(userId, params);
					break;
				case 'saveContext':
					this.checkAdminAndRun(userId, this.saveContext.bind(this));
					break;
			}
		} catch(e) {
			debug('some shit happened on command handling:', e);
		}
	}

	turnOnOffSignal(userId, signalId, ready) {
		const userContext = this.getUserContext(userId);
		signalId = parseInt(signalId);
		if(!this.checkSignalId(userId, signalId))
			return;
		const signal = userContext.signals[signalId];
		signal.ready = ready;
		this.answer(userId, `signal updated:\n${signalId}: ${signal.toString()}`);
	}

	checkAdminAndRun(userId, fn) {
		debug('admin command requested, userId:', userId, 'function:', fn);
		if(this.context.admins[userId]) {
			fn();
			this.answer(userId, `command ${fn.toString()} executed`)
		}
		else
			this.answer(userId, 'not authorized');
	}

	help(userId, params) {
		if(params.length && this.commandHelp[params[0]]) {
			this.answer(userId, this.commandHelp[params[0]]);
		}
		else {
			if(params[0] == 'signalType') {
				this.answer(userId, 'available signal types:\n\n' +
					Object.entries(Signals).map(([signalTypeName, signalClass]) =>
					`${signalTypeName}: ${signalClass.description()}`).join('\n\n'));
			}
			else
				this.answer(userId, 'available commands:\n\n' + Object.values(this.commandHelp).join('\n'));
		}
	}

	newSignal(userId, signalType, params) {
		const userContext = this.getUserContext(userId);
		const signalId = userContext.signals.length;
		try {
			const signal = new Signals[signalType](params, this.onTriggerClosure(userId, signalId), true);
			userContext.signals.push(signal);
			this.answer(userId, 'added new signal:\n' + signal.toString());

		} catch(e) {
			debug('newSignal():', signalType, params, e);
			this.answer(userId, `unable to create new signal of type "${signalType}" with parameters ${params}`);
		}
	}

	changeSignal(userId, signalId, params) {
		const userContext = this.getUserContext(userId);
		signalId = parseInt(signalId);
		if(!this.checkSignalId(userId, signalId))
			return;
		const signal = userContext.signals[signalId];
		signal.change(params);
		this.answer(userId, `signal updated:\n${signalId}: ${signal.toString()}`);
	}

	checkSignalId(userId, signalId) {
		const userContext = this.getUserContext(userId);
		if(isNaN(signalId) || signalId < 0 || signalId >= userContext.signals.length) {
			debug('changeSignal(): bad signalId:', signalId);
			this.answer(userId, 'signal id out of range. current signals:');
			this.listSignals(userId);
			return false;
		}
		return true;
	}

	deleteSignal(userId, signalId) {
		const userContext = this.getUserContext(userId);
		signalId = parseInt(signalId);
		if(!this.checkSignalId(userId, signalId))
			return;
		userContext.signals.splice(signalId, 1);
		this.answer(userId, `signal #${signalId} deleted`);
		this.listSignals(userId);
	}

	listSignals(userId) {
		const userContext = this.getUserContext(userId);
		if(!userContext.signals.length) {
			this.answer(userId, 'no signals');
			return;
		}
		this.answer(userId, userContext.signals.map((signal, idx) =>
			`${idx}: current value: [${signal.getCurrentValue()}], state: ${signal.toString()}`).join('\n'));
	}

	async loadContext(contextFilepath) {
		this.contextFilepath = contextFilepath;
		try {
			const json = await fsp.readFile(contextFilepath, {encoding: 'utf8'});
			this.context = JSON.parse(json);
			for(const userId in this.context.users) {
				const user = this.getUserContext(userId);
				user.signals = user.signals.map((sig, signalId) =>
					new Signals[sig.type](sig.params, this.onTriggerClosure(userId, signalId), sig.ready));
			}
			debug('context loaded successfully');

		} catch(e) {
			console.error('unable to load context:', e);
			this.emptyContext();
		}
	}

	onTriggerClosure(userId, signalId) {
		return (value) => {
			this.onSignalTrigger(userId, signalId, value);
		}
	}

	onSignalTrigger(userId, signalId, value) {
		this.answer(userId, `signal #${signalId} triggered! current value is:\n${value}`)
	}

	emptyContext() {
		this.context = {
			users: {},
			admins: {}
		};
		debug('empty context loaded');
	}

	getUserContext(userId) {
		if(!this.context.users[userId]) {
			this.context.users[userId] = {
				signals: []
			};
		}
		return this.context.users[userId];
	}

	async saveContext(contextFilepath) {
		try {
			await fsp.writeFile(contextFilepath || this.contextFilepath || './default-context.json',
				JSON.stringify(this.context, null, '\t'), {encoding: 'utf8'});
			debug('context saved successfully');
		} catch(e) {
			console.error('unable to save context:', e);
		}
	}
}

module.exports = Bot;
