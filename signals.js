'use strict';

const debug = require('debug')('signals');
const TimeWindow = require('./time-window');
const Operations = require('./math');
const { strToMs } = require('./utility');

class SignalBase {
	static paramDefaults = {};
	static annotation = 'abstract signal class';
	static description() {
		return this.annotation + '\nparameter names and defaults: ' + Object.entries(this.paramDefaults).map(([name, defaultValue]) => `${name}=${defaultValue}`).join(' ');
	}
	constructor(params, onTrigger, ready) {
		this.type = 'abstract';
		debug('onTrigger =', onTrigger.toString(), 'ready =', ready);
		this.onTrigger = onTrigger;
		this.ready = ready;
		this.params = this.paramsToObject(params);
	}

	change(params) {
		this.params = this.paramsToObject(params, this.params);
	}

	onNewValue() {
		if(this.ready)
			this.check();
	}

	getCurrentValue() {
		return null;
	}

	toString() {
		const paramStr = Object.entries(this.params).map(entry => `${entry[0]}:${entry[1]}`).join(' ');
		return `${this.ready ? 'on' : 'off'} ${this.type} ${paramStr}`;
	}

	static fromString(str, onTrigger) {
		const list = str.split(' ');
		params = Object.fromEntries(list.slice(1).map(paramStr => paramStr.split(':')));
		return new module.exports[list[0]](params, onTrigger);
	}

	// on error throws RangeError
	paramsToObject(paramsList, defaults = this.constructor.paramDefaults) {
		const result = Object.assign({}, defaults);
		if(!(paramsList instanceof Array))
			return this.convertNumericParams(Object.assign(result, paramsList));
		if(!paramsList.length)
			return result;
		if(paramsList[0].includes(':')) {
			// named params
			for(let param of paramsList) {
				const [ name, value ] = param.split(':', 2);
				if(value === undefined) {
					debug('value expected for parameter', name);
					throw new RangeError(`value expected for parameter ${name}`);
				}
				result[name] = value;
			};
		}
		else {
			// ordered params
			const names = Object.keys(this.constructor.paramDefaults);
			if(paramsList.length > names.length) {
				debug('too many parameters');
				throw new RangeError('too many parameters');
			}
			for(let i = 0; i < paramsList.length; i++) {
				result[names[i]] = paramsList[i];
			}
		}
		return this.convertNumericParams(result);
	}

	convertNumericParams(params) {
		const result = Object.assign({}, params);
		for(const name in this.constructor.paramDefaults) {
			if(typeof this.constructor.paramDefaults[name] == 'number')
				result[name] = new Number(params[name]).valueOf();
		}
		return result;
	}
}

class SimplePriceLimit extends SignalBase {
	static paramDefaults = {
		targetMinPrice: 0,
		targetMaxPrice: 1e6,
		windowSize: '1m'
	};
	static annotation = 'detects average price outside given boundaries';

	#window = null;

	constructor(params, onTrigger, ready) {
		super(params, onTrigger, ready);
		this.type = this.constructor.name;
		this.#window = new TimeWindow(this.params.windowSize, new Operations.TradeOperations())
	}

	change(params) {
		const oldWindowSize = strToMs(this.params.windowSize);
		super.change(params);
		if(oldWindowSize != strToMs(this.params.windowSize))
			this.#window.resize(this.params.windowSize);
		if(this.ready)
			this.check();
	}

	onNewValue(trade) {
		this.#window.add(trade, trade.tradeTime);
		super.onNewValue();
	}

	getCurrentValue() {
		const value = new Operations.PricesTuple(this.#window.getTotals().ohlc);
		return value;
	}

	check() {
		const currentValue = this.getCurrentValue();
		const price = currentValue.wghtd;
		if(price === null)
			return;
		if(price > this.params.targetMaxPrice || price < this.params.targetMinPrice) {
			debug('check positive!');
			this.ready = false;
			this.onTrigger(currentValue);
		}
	}
}

module.exports = { SimplePriceLimit };
