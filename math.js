'use strict';

const debug = require('debug')('math');

const { usd } = require('./utility');

class TradeOperations {
	constructor() {
		this.accumulator = {
			price: 0,
			quantityBuy: 0,
			quantitySell: 0,
			volumeBuy: 0,
			volumeSell: 0,
			minmax: {
				high: 0,
				low: Number.MAX_VALUE
			}
		};
	}

	validate(value) {
		return !isNaN(value.price) && !isNaN(value.quantity);
	}

	onAdd(addedValue, sequence) {
		this.accumulator.price += addedValue.price;
		if(addedValue.isBuyerMM) {
			this.accumulator.quantitySell += addedValue.quantity;
			this.accumulator.volumeSell += addedValue.volume;
		} else {
			this.accumulator.quantityBuy += addedValue.quantity;
			this.accumulator.volumeBuy += addedValue.volume;
		}
		this.accumulator.minmax.high = Math.max(this.accumulator.minmax.high, addedValue.price);
		this.accumulator.minmax.low = Math.min(this.accumulator.minmax.low, addedValue.price);
	}

	onRemove(removedValue, sequence) {
		this.accumulator.price -= removedValue.price;
		if(removedValue.isBuyerMM) {
			this.accumulator.quantitySell -= removedValue.quantity;
			this.accumulator.volumeSell -= removedValue.volume;
		} else {
			this.accumulator.quantityBuy -= removedValue.quantity;
			this.accumulator.volumeBuy -= removedValue.volume;
		}
		if(removedValue.price == this.accumulator.minmax.high || removedValue.price == this.accumulator.minmax.low) {
			this.accumulator.minmax.high = 0;
			this.accumulator.minmax.low = Number.MAX_VALUE;
			if(sequence.length)
				sequence.forEach(element => {
					this.accumulator.minmax.high = Math.max(this.accumulator.minmax.high, element.value.price);
					this.accumulator.minmax.low = Math.min(this.accumulator.minmax.low, element.value.price);
				});
		}
	}

	getTotals(sequence) {
		const result = {};
		Object.assign(result, this.accumulator);
		result.price /= (sequence.length || 1);
		delete result.minmax;
		result.quantity = result.quantityBuy + result.quantitySell;
		result.volume = result.volumeBuy + result.volumeSell;
		result.windowSize = sequence.length;
		result.ohlc = sequence.length ? {
			open:  sequence[0].value.price,
			high:  this.accumulator.minmax.high,
			low:   this.accumulator.minmax.low,
			close: sequence[sequence.length-1].value.price,
			mean:  result.price,
			wghtd: result.volume / (result.quantity || 1) * 1e8
		} : {
			open: null, high: null, low: null, close: null, mean: null, wghtd: null
		};
		return result;
	}
}

class Dummy {
	constructor() {}
	validate(value) {}
	onAdd(addedValue, sequence) {}
	onRemove(removedValue, sequence) {}
	getTotals(sequence) {}
}

class PricesTuple {
	constructor(values) {
		Object.assign(this, values);
	}
	toString() {
		return Object.entries(this)
			.map(([name, value]) => ({ name, value }))
			.filter(obj => typeof obj.value == 'number')
			.map(price => `${price.name}: ${usd(price.value, 4)}`)
			.join(', ');
	}
}

class PriceOperations {
	constructor() {
		this.accumulator = {
			sum: 0,
			high: 0,
			low: Number.MAX_VALUE
		}
	}

	validate(value) {
		return (typeof value == 'number') && !isNaN(value);
	}

	onAdd(addedValue, sequence) {
		this.accumulator.sum += addedValue;
		this.accumulator.high = Math.max(this.accumulator.high, addedValue);
		this.accumulator.low = Math.min(this.accumulator.low, addedValue);
	}

	onRemove(removedValue, sequence) {
		this.accumulator.sum -= removedValue;
		if(removedValue == this.accumulator.high || removedValue == this.accumulator.low) {
			debug('removing local extremum', removedValue, ' high:', this.accumulator.high, 'low:', this.accumulator.low);
			this.accumulator.high = 0;
			this.accumulator.low = Number.MAX_VALUE;
			if(sequence.length)
				sequence.forEach(value => {
					this.accumulator.high = Math.max(this.accumulator.high, value.value);
					this.accumulator.low = Math.min(this.accumulator.low, value.value);
				});
		}
	}

	getTotals(sequence) {
		return new PricesTuple({
			open:  sequence.length ? sequence[0].value : null,
			high:  this.accumulator.high,
			low:   this.accumulator.low,
			close: sequence.length ? sequence[sequence.length-1].value : null,
			mean:  sequence.length ? this.accumulator.sum / sequence.length : null
		});
	}
}

module.exports = { Dummy, PricesTuple, TradeOperations, PriceOperations };
